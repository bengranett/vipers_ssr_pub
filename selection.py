

import numpy as N


def select(mode, zflag):
    """Define the target selection flags"""

    zflg = int(zflag)
    zqual = zflg%10

    if mode=='galaxy':
        if (zqual >= 2)&(zflg<10)&(zflg>0):
            return True
    elif mode=='galaxyagn':
        if (zqual >= 2)&(zflg<20)&(zflg>0):
            return True
    elif mode=='galaxylow':
        if (zflag >= 1.5)&(zflg<10)&(zflg>0):
            return True
    else:
        print "what?",mode
        sys.exit(-1)



def star(zflag,zspec):
    """Define the star selection"""
    zflg = int(zflag)
    zqual = zflg%10

    if (zqual >= 2)&(zflg<20)&(zflg>0)&(N.abs(zspec)<1e-3):  # a star
        return True
    return False
