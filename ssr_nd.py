import numpy as N

from scipy.spatial import cKDTree

import time
import githeader
import selection
import params

def do_count_list(tree, data, r):
    count = []
    for i in range(len(data)):
        m = tree.query_ball_point(data[i],r[i])
        count.append(len(m))
    count = N.array(count)
    return count
       
def get_rmax(tree, k=100):
    """ """
    d,m = tree.query(tree.data, k)

    rmax = N.zeros(len(d))
    for i in range(len(d)):
        rmax[i] = N.max(d[i])

    print "rmax",rmax[:10]

    return rmax





def go(knn=None):
    """ """
    if knn == None:
        knn = params.knn

    data = []
    objclass = []
    num = []

    for line in file("table_pval.txt"):
        line = line.strip()
        if line.startswith("#"):
            if "configuration" in line:
                config = line.split()[-1]
                if not config == params.mode:
                    print "Configuration in table_pval.txt is inconsistent with params. Do it correctly."
                    sys.exit(-1)
            continue
                    
        w = line.split()
        
        num.append(w[0])

        zflg = float(w[1])
        features = [float(v) for v in w[2:]]

        data.append(features)
        
        if selection.select(params.mode, zflg):
            c = 1
        else:
            c = 0

        objclass.append(c)

    data = N.array(data)
    objclass = N.array(objclass)
    n,dim = data.shape

    print "> length",n
    print "> dimens",dim

    tree_targ = cKDTree(data)
    tree_succ = cKDTree(data[objclass==1])

    rmax = get_rmax(tree_targ,k=knn)
    print "rmax",N.percentile(rmax,[10,50,90])

    print "query 1"
    count = do_count_list(tree_targ, tree_targ.data, rmax)
    print len(count),N.median(count),N.percentile(count,[10,90])

    print "query 2"
    count2 = do_count_list(tree_succ, tree_targ.data, rmax)
    print len(count2),N.median(count2),N.percentile(count2,[10,90])

    w = count2*1./count

    out = file("result_n%i_%i.txt"%(dim,knn),"w")
    print >>out, "#",time.asctime()
    print >>out, "# commit",githeader.gitcommit()
    print >>out, "# configuration",params.mode
    print >>out, "# knn",knn
    for i in range(len(count)):
        print >>out, num[i],count[i],count2[i],w[i]
    out.close()
    
    print  ">> Output: result_n%i_%i.txt"%(dim,knn)

go()
