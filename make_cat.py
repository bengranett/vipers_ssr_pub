import matplotlib
#matplotlib.rc_context(rc={'backend': 'PDF'})
matplotlib.rc_context(rc={'backend': 'GTKAgg'})
import numpy as N
import pylab
import githeader
import time
import selection

from tornado import database
db=database.Connection("frontend", "vipers",user="",password="")


feature_list = "photo_z,M_Uj,M_B,M_V,M_R,M_I,M_Ks,M_FUV,M_NUV,log_SFR,log_stellar_mass".split(",")

def load_absmags(field='W1'):

    features = ",".join(feature_list)
    query = "select num,%s from PHOT_T07_SED_FITTING;"%features
    print query

    data = {}
    results = db.query(query)
    for r in results:
        num = r['num']
        vec = []
        for k in feature_list:
            vec.append(r[k])
        data[num] = vec

    return data

def load_mean_ssr():
    """ """
    configuration = "unknown"
    data = {}
    for line in file('ssr_by_quad.txt'):
        line = line.strip()
        if line.startswith("#"): 
            if "configuration" in line:
                configuration = line.split()[-1]
            continue
        w = line.split()
        data[w[0]] = float(w[-2])

    return data,configuration



absmags_d = load_absmags()
ssr_d, configuration = load_mean_ssr()


query = """select S.num,zflg,pointing,quadrant,S.zspec,S.selmag,
P.u_T07, P.g_T07, P.r_T07, P.i_T07,  P.iy_T07, P.z_T07 
from  SPECTRO_V7_0 as S 
join PHOT as P on S.num=P.num 
where P.vmmpsflag='S' 
  and P.newflag=1 
  and zflg >= 0 
  and zflg < 20;"""

print query
results = db.query(query)

table = []
data = []


labels = "num","zflg","Q_quad","i_AB","photo_z","M_FUV","M_NUV","M_Uj","M_B","M_V","M_R","M_I","M_Ks","M_NUV-M_R","M_U-M_V","M_R-M_I","log_SFR","log_stellar_mass"


out = file("table.txt","w")
print >>out, "#",time.asctime()
print >>out, "# commit",githeader.gitcommit()
print >>out, "# configuration", configuration
print >>out,"#",
for l in labels:
    print >>out, l,
print >>out,""

for r in results:
    num = r['num']
    zspec = r['zspec']
    zflg = r['zflg']

    if selection.star(zflg, zspec):
        continue

    imag = r['selmag']
    umag = r['u_T07']
    gmag = r['g_T07']
    rmag = r['r_T07']
    imag = r['i_T07']
    iymag = r['iy_T07']
    zmag = r['z_T07']
    selmag = r['selmag']
    point = r['pointing']
    quad = r['quadrant']

    if imag<-90: imag=iymag
    if imag<-90: print "warning! i mag:",imag,iymag

    if point.startswith("W4P9"):
        point = "W4P0"+point[4:]
    if point.startswith("W1P9"):
        point = "W1P1"+point[4:]

    p = "%sQ%s"%(point,quad)

    field = int(point[1:2])

    ssr = ssr_d[p]

    red,u,b,v,mr,mi,k,fuv,nuv,sfr,mstar = absmags_d[num]

    vec = (num,zflg,ssr,imag,b,u-v)
    print >>out, num,zflg,zspec,ssr,selmag,red,fuv,nuv,u,b,v,mr,mi,k,nuv-mr,u-v,mr-mi,sfr,mstar
    table.append(vec)
    data.append(vec[2:])


out.close()


data = N.transpose(data)
datas = []
orders = []
for v in data:
    o = v.argsort()
    orders.append(o)
    datas.append(v[o])


stretch = N.ones(len(table[0]))

out = file("table_pval.txt","w")
print >>out, "#",time.asctime()
print >>out, "# commit",githeader.gitcommit()
print >>out, "# configuration", configuration

for v in table:
    print >>out, v[0],v[1],
    x = v[2:]
    for i in range(len(x)):
        p = N.searchsorted(datas[i],x[i])*1./len(datas[i])
        print >>out, p*stretch[i],
    print >>out, ""

out.close()

pgauss = []
for o in orders:
    r = N.random.normal(0,1,len(o))
    r.sort()
    p = N.zeros(len(r))
    p[o] = r
    pgauss.append(p)

out = file("table_pval_gauss.txt","w")
print >>out, "#",time.asctime()
print >>out, "# commit",githeader.gitcommit()
print >>out, "# configuration", configuration

c = 0
for v in table:
    print >>out, v[0],v[1],
    for i in range(len(x)):
        print >>out, pgauss[i][c],
    print >>out, ""
    c += 1


out.close()
