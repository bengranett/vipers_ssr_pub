# README #

## Options ##
Options are in ```params.py```
```
#!python
mode = "galaxylow"
knn = 100
```

The ```mode``` parameter describes the sample selection.  It should be clear looking at the code in ```selection.py```:
```
#!python
    if mode=='galaxy':
        if (zqual >= 2)&(zflg<10)&(zflg>0):
            return True
    elif mode=='galaxyagn':
        if (zqual >= 2)&(zflg<20)&(zflg>0):
            return True
    elif mode=='galaxylow':
        if (zflag >= 1.5)&(zflg<10)&(zflg>0):
            return True
```


## Running ##
```
#!bash
# compute mean SSR per quadrant
python ssr_by_quadrant.py

# build catalog of parameters
python make_cat.py

# compute SSR
python ssr_nd.py

# make a plot
python plot_ssr.py

```