import matplotlib
#matplotlib.rc_context(rc={'backend': 'PDF'})
matplotlib.rc_context(rc={'backend': 'GTKAgg'})
import numpy as N
import pylab
import time

import githeader

import selection
import params

from tornado import database
db=database.Connection("frontend", "vipers",user="",password="")



query = """select zflg,pointing,quadrant,zspec 
from  SPECTRO_V7_0 as S 
join PHOT as P on S.num=P.num 
where P.vmmpsflag='S' 
  and P.newflag=1 
  and zflg >= 0 
  and zflg < 20;"""

results = db.query(query)

count = {}
starcount = {}

for r in results:
    zflg = r['zflg']

    point = r['pointing']
    quad = r['quadrant']
    zspec = r['zspec']

    if point.startswith("W4P9"):
        point = "W4P0"+point[4:]

    if point.startswith("W1P9"):
        point = "W1P1"+point[4:]

    p = "%sQ%s"%(point,quad)

    if not count.has_key(p):
        count[p] = [0,0]

    if not starcount.has_key(p):
        starcount[p] = 0

    count[p][0]+=1

    if selection.select(params.mode, zflg):
        count[p][1]+=1
    
    if selection.star(zflg,zspec):
        starcount[p] += 1


out = file("ssr_by_quad.txt","w")
print >>out, "#",time.asctime()
print >>out, "# commit",githeader.gitcommit()
print >>out, "# configuration",params.mode

points = N.array(count.keys())

ssr_s = []
for p in points:
    ssr_s.append((count[p][1]-starcount[p])*1./(count[p][0]-starcount[p]))

ssr_s = N.array(ssr_s)
order = ssr_s.argsort()

ssr_s = ssr_s[order]
points = points[order]

print >>out,"""# pointing, N_targ, N_success, N_star, mean SSR, rank"""

for i,p in enumerate(points):
    ind = N.searchsorted(ssr_s, ssr_s[i])
    print >>out, p, count[p][0],count[p][1],starcount[p],ssr_s[i],ind

out.close()
