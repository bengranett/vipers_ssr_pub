import git



def gitcommit():
    # print the commit hash for this code
    repo = git.Repo(".",search_parent_directories=True)
    return repo.head.commit.hexsha
