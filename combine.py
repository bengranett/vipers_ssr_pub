
import time
import githeader

dict = {}
for tag in ['galagn','gal','gallow']:
    for line in file("results_%s/result_n4_100.txt"%tag):
        if line.startswith("#"): continue
        w = line.split()
        id = w[0]
        if not dict.has_key(id):
            dict[id] = {}
        dict[id][tag] = w[-1]


now = time.asctime()

out = file("comb.txt","w")
print >>out, """# vipers ssr by sample
# generated %s
# %s
# 
# col1 ID
# col2 SSR galaxies+agn    (2<zflg<10 or 12<zflg<20)
# col3 SSR galaxies only   (2<zflg<10)
# col4 SSR galaxies-zphot  (1.5<=zflg<10)"""%(now,githeader.gitcommit())


keys = dict.keys()

keys.sort()
for id in keys:
    line = id
    for tag in ['galagn','gal','gallow']:
        line += "\t%15s"%dict[id][tag]
    line += "\n"
    out.write(line)

out.close()
